var scanner = require('./PoGoScanner.js');
var auth    = require('./auth.js');

var coords = {
    latitude: 10.301335,
    longitude: 123.962714,
    altitude: 0
};

scanner(auth, coords);
