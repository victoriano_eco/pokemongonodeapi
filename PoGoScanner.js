var fs        = require('fs');
var moment    = require('moment');
var PokemonGO = require('pokemon-go-node-api');

function PoGoScanner(auth, coords) {
    var PokeIO = new PokemonGO.Pokeio();

    var location = {
        type: 'coords',
        coords: coords
    };

    PokeIO.init(auth.username, auth.password, location, auth.provider, function(err) {
        if (err) throw err;

        console.log('[i] Current location: ' + PokeIO.playerInfo.locationName);
        console.log('[i] lat/long/alt: : ' + PokeIO.playerInfo.latitude + ' ' + PokeIO.playerInfo.longitude + ' ' + PokeIO.playerInfo.altitude);

        PokeIO.GetProfile(function(err, profile) {
            if (err) throw err;
            const pokemonlist = PokeIO.pokemonlist;

            setInterval(function() {
                PokeIO.Heartbeat(function(err, poke_data) {
                    if(err) console.log(err);

                    var pokemons = [];
                    console.log('*****************************************');

                    if(typeof poke_data != 'undefined' && typeof poke_data['cells'] != 'undefined') {
                        for (var i in poke_data['cells']) {
                            var cell = poke_data['cells'][i];
                            var wild = cell.WildPokemon;
                            var map = cell.MapPokemon;
                            var nearby = cell.NearbyPokemon;

                            if (nearby.length > 0) {
                                // var poke_file = './poke_data/poke_data_' + moment().format('YYYYMMDDHHmmss') + '.json';
                                // fs.writeFile(poke_file, JSON.stringify(cell), function (err) {
                                //     if (err) {
                                //         // console.log('An error occured : ' + err);
                                //     } else {
                                //         // console.log('poke_file saved : ' + poke_file);
                                //     }
                                // });
                                
                                for (var j in nearby) {
                                    var pid = parseInt(nearby[j].PokedexNumber);
                                    var name = pokemonlist[pid - 1].name;
                                    var location = {
                                        lat: typeof cell.MapPokemon[j] !== 'undefined' ? cell.MapPokemon[j]['Latitude'] : null,
                                        long: typeof cell.MapPokemon[j] !== 'undefined' ? cell.MapPokemon[j]['Longitude'] : null
                                    };
                                    var ms = typeof wild[j] != 'undefined' && typeof wild[j].TimeTillHiddenMs != 'undefined' ? wild[j].TimeTillHiddenMs : 0;
                                    var poke = {
                                        pid: ('000' + pid).slice(-3),
                                        name: name,
                                        // location: location,
                                        hidden: ms, // moment.utc(ms).format("HH:mm:ss.SSS"),
                                        formatted: [location.lat, location.long].join(',')
                                    };

                                    console.log(poke.pid + ':' + poke.name + '; h=' + poke.hidden + '; loc=' + poke.formatted);
                                }
                            }
                        }
                    }
                });
            }, 20000);
        });
    });
}

module.exports = PoGoScanner;